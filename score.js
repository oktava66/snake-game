(function() {
  const STORAGE_KEY = 'snakeGameScore';

  /** Representing total score */
  class Score {
    /**
     * Initialize score. ".score" and "#scoreValue" must exist in DOM
     */
    constructor() {
      this.scoreElement = document.querySelector('.score');
      this.scoreValueElement = document.querySelector('#scoreValue');
    }

    /**
     * Initialize the score with default values
     */
    init() {
      this.score = 0;
      this.topScore = +localStorage.getItem(STORAGE_KEY) || 0;
      this.topScoreDefeated = false
      this.scoreElement.classList.remove('top');
      this.scoreValueElement.innerHTML = this.score;
    }

    /**
     * Increase score by value and change color of score element, if previous score was beaten
     * @param {number} value
     */
    add(value) {
      this.score += value;
      this.scoreValueElement.innerHTML = this.score;

      if (!this.topScoreDefeated && this.score > this.topScore) {
        this.topScoreDefeated = true;
        this.scoreElement.classList.add('top');
      }
    }

    /**
     * Save score to storage, if it is greater than previous
     */
    saveScore() {
      if (this.topScoreDefeated) {
        this.topScore = this.score;
        localStorage.setItem(STORAGE_KEY, this.score);
      }
    }
  }

  // Expose Snake class to global scope
  window.game.Score = Score;
})();
