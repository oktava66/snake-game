(function() {
  // Used to prevent reverse direction and allow only right or left turn
  const REVERSE_DIRECTIONS = {
    top: 'bottom',
    right: 'left',
    bottom: 'top',
    left: 'right',
  };
  Object.freeze(REVERSE_DIRECTIONS);

  /** Representing a snake */
  class Snake {
    /**
     * Initialize snake
     * @param {number} blockSize Size of point on game canvas
     */
    constructor(blockSize) {
      this.blockSize = blockSize;
      this.parts = [];
    }

    /**
     * Initialize the snake with default direction and position
     */
    init() {
      const parts = this.parts;

      parts.length = 0;
      parts.push([400, 400]);
      parts.push([400, 416]);
      parts.push([400, 432]);

      this.direction = 'right';
    }

    /**
     * Reverse the snake direction and check for collision with boundaries
     * @param {Boundaries} boundaries
     * @returns {bool} If collision with a boundary exists
     */
    reverse(boundaries) {
      const head = this.parts[0];
      const neck = this.parts[1];

      // set reverse direction
      // horizontal
      if (head[0] !== neck[0]) {
        if (head[0] > neck[0]) {
          this.direction = 'right';
        } else {
          this.direction = 'left';
        }
      }
      // vertical
      if (head[1] !== neck[1]) {
        if (head[1] > neck[1]) {
          this.direction = 'bottom';
        } else {
          this.direction = 'top';
        }
      }

      this.parts.reverse();

      return boundaries.checkCollision(this.parts[this.parts.length - 1]);
    }

    /**
     * Try to set new direction. If not reversed, set it
     * @param {string} newDirection
     */
    setDirection(newDirection) {
      if (newDirection && newDirection !== REVERSE_DIRECTIONS[this.direction]) {
        this.direction = newDirection;
      }
    }

    /**
     * Get next step of the snake, considering it's direction
     * @returns {array} [x, y]
     */
    getNextStep() {
      const head = this.parts[this.parts.length - 1];

      if (this.direction === 'top') {
        return [head[0], head[1] - this.blockSize];
      } else if (this.direction === 'right') {
        return [head[0] + this.blockSize, head[1]];
      } else if (this.direction === 'bottom') {
        return [head[0], head[1] + this.blockSize];
      } else if (this.direction === 'left') {
        return [head[0] - this.blockSize, head[1]];
      }
    }

    /**
     * Make a step to passed place and check for collision of snake with itself
     * @param {array} nextStep [x, y]
     * @returns {bool} If collision with itself exists
     */
    makeStep(nextStep) {
      this.parts.shift();
      const selfCollision = this.parts.find(position => (
        nextStep[0] === position[0] && nextStep[1] === position[1]
      ));
      this.parts.push(nextStep);
      return !!selfCollision;
    }

    /**
     * Increase length of the snake by one
     */
    grow() {
      this.parts.unshift(this.parts[0]);
    }

    /**
     * Draw the snake
     * @param ctx Canvas context
     */
    draw(ctx) {
      const lastIndex = this.parts.length - 1;

      this.parts.forEach((position, index) => {
        ctx.beginPath();
        ctx.rect(position[0] + 2, position[1] + 2, this.blockSize - 4, this.blockSize - 4);
        ctx.fillStyle = index === lastIndex ? '#000066' : '#4388cc';
        ctx.fill();
        ctx.lineWidth = 1;
        ctx.strokeStyle = '#000066';
        ctx.stroke();
        ctx.closePath();
      });
    }
  }

  // Expose Snake class to global scope
  window.game.Snake = Snake;
})()