(function() {
  // Constants
  const CANVAS_WIDTH = 800;
  const CANVAS_HEIGHT = 800;
  const BLOCK_SIZE = 16;
  const SPECIAL_FOOD_SIZE = 28;
  const KEY_TO_DIRECTION = {
    38: 'top',
    39: 'right',
    40: 'bottom',
    37: 'left',
  };
  Object.freeze(KEY_TO_DIRECTION);

  // Canvas initiation
  const canvasElement = document.getElementById('gameCanvas');
  const ctx = canvasElement.getContext('2d');
  ctx.canvas.width = CANVAS_WIDTH;
  ctx.canvas.height = CANVAS_HEIGHT;

  // Game controls
  const startButtonElement = document.querySelector('.startButton');
  let nextDirection = null; // next direction of snake
  let stopped = false; // is game stopped

  // Game objects
  const score = new game.Score();
  const snake = new game.Snake(BLOCK_SIZE);
  const boundaries = new game.Boundaries();
  const food = new game.Food(BLOCK_SIZE, BLOCK_SIZE);
  const specialFood = new game.SpecialFood(BLOCK_SIZE, SPECIAL_FOOD_SIZE);

  // Move snake by one step forward and check all possible collisions
  function moveSnake() {
    snake.setDirection(nextDirection);
    nextDirection = null;

    const nextStep = snake.getNextStep();
    const boundaryCollision = boundaries.checkCollision(nextStep);

    // Snake reached one of the boundaries, so reverse snake
    if (boundaryCollision) {
      // If after reverse snake has collision with a boundary, so game over :(
      const boundaryCollisionAfterReverse = snake.reverse(boundaries);
      if (boundaryCollisionAfterReverse) {
        return stopGame();
      }

      food.put(boundaries);
      specialFood.put(boundaries);
      return;
    }

    // Keep moving forward

    const selfCollision = snake.makeStep(nextStep);

    if (selfCollision) {
      return stopGame();
    }

    // Check for collision with food
    if (food.checkCollision(nextStep[0], nextStep[1])) {
      score.add(1);
      food.put(boundaries);
      snake.grow();
    }

    // Check for collision with special food
    if (specialFood.checkCollision(nextStep[0], nextStep[1])) {
      score.add(9);
      specialFood.put(boundaries);
      snake.grow();
      snake.grow();
    }
  }

  // Used to check when snake needs a movement
  const time = {
    start: null,
    elapsed: null,
    level: null, // Interval between moves
  };

  /**
   * RAF tick handler
   * @param {time} now RAF's timer
   */
  function mainLoop(now) {
    // Check for time past after last snake step
    time.elapsed = now - time.start;
    if (time.elapsed > time.level) {
      // Reset timer and move snake
      time.start = now;
      moveSnake();
    }

    // Clear canvas to get empty frame
    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    // Draw objects on canvas
    boundaries.draw(ctx);
    snake.draw(ctx);
    food.draw(ctx);
    specialFood.draw(ctx);

    // If game stopped, do not subscribe for next animation frame
    if (stopped) {
      return;
    };

    window.requestAnimationFrame(mainLoop);
  }

  /**
   * Set game objects and controllers to default state and start RAF loop
   */
  function startGame() {
    // Set game objects to default
    score.init();
    snake.init();
    boundaries.init(CANVAS_WIDTH, CANVAS_HEIGHT);
    food.put(boundaries);
    specialFood.put(boundaries);

    // Set controllers to default
    nextDirection = null;
    stopped = false;
    time.start = 0;
    time.elapsed = 0;
    time.level = 100;
    stopped = false;

    // Hide start button
    startButtonElement.classList.add('hidden');
    startButtonElement.innerHTML = 'Play Again';

    // Start RAF
    window.requestAnimationFrame(mainLoop);
  }

  /**
   * Stop game, show restart button and save score to storage
   */
  function stopGame() {
    stopped = true;
    startButtonElement.classList.remove('hidden');
    score.saveScore();
  }

  // Key down event listener to change snake's direction
  window.addEventListener('keydown', (e) => {
    nextDirection = KEY_TO_DIRECTION[e.keyCode];
  });

  // Expose start function to global scope to reach it from button click handler
  window.startGame = startGame;
})();
