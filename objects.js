(function() {
  const BOUNDARY_TOUCH_PENALTY = 113;
  const SPECIAL_FOOD_COLORS = ['#abcdef', '#efa0a6', '#bdefab', '#efe371', '#efa8eb'];
  Object.freeze(SPECIAL_FOOD_COLORS);

  /**
   * Get random integer
   * @param {number} min
   * @param {number} max
   * @returns {number}
   */
  function getRandomInt(min, max) {
    return Math.round(Math.random() * (max - min) ) + min;
  }

  /** Representing an area, which snake can not access */
  class Boundaries {
    /**
     * Initialize boundary's default dimensions
     * @param width Width of a canvas
     * @param height Height of a canvas
     */
    init(width, height) {
      this.canvasWidth = width;
      this.canvasHeight = height;

      this.top = 0;
      this.right = width;
      this.bottom = height;
      this.left = 0;
    }

    /**
     * Checks for collision of the boundary with passed point
     * @param nextStep [x, y]
     * @returns {string|boolean}
     */
    checkCollision(nextStep) {
      if (nextStep[1] < this.top) {
        this.bottom -= BOUNDARY_TOUCH_PENALTY;
        return 'top';
      } else if (nextStep[0] >= this.right) {
        this.left += BOUNDARY_TOUCH_PENALTY;
        return 'right';
      } else if (nextStep[1] >= this.bottom) {
        this.top += BOUNDARY_TOUCH_PENALTY;
        return 'bottom';
      } else if (nextStep[0] < this.left) {
        this.right -= BOUNDARY_TOUCH_PENALTY;
        return 'left';
      }
      return false;
    }

    /**
     * Draw the boundary
     * @param ctx Canvas context
     */
    draw(ctx) {
      ctx.globalCompositeOperation = 'xor';
      ctx.fillStyle = '#c0c0c0';
      ctx.fillRect(0, 0, this.canvasWidth, this.canvasHeight);

      ctx.fillStyle = '#000000';
      ctx.fillRect(this.left, this.top, this.right - this.left, this.bottom - this.top);
      ctx.globalCompositeOperation = 'source-over';
    }
  }

  /** Representing an eatable object */
  class Eatable {
    /**
     * Create an eatable object
     * @param {number} blockSize Size of point on game canvas
     * @param {number} size Size of eatable object
     * @param {array} timeouts Min and max values of timeout
     * @param {number} boundaryGutter Distance from boundaries (in case if an eatable object larger than point)
     * @param {bool} colored Need to fill with a color
     */
    constructor(blockSize, size, timeouts, boundaryGutter, colored) {
      this.blockSize = blockSize;
      this.size = size;
      this.timeouts = timeouts;
      this.boundaryGutter = boundaryGutter;
      this.colored = colored;
    }

    /**
     * Generate random coordinates of eatable object and start recursive
     * generation of eatable object with random timeouts
     * @param {Boundaries} boundaries Boundaries object
     */
    put(boundaries) {
      this.x = getRandomInt(
        Math.ceil(boundaries.left / this.blockSize) + this.boundaryGutter,
        Math.floor(boundaries.right / this.blockSize) - this.boundaryGutter,
      ) * this.blockSize;
      this.y = getRandomInt(
        Math.ceil(boundaries.top / this.blockSize) + this.boundaryGutter,
        Math.floor(boundaries.bottom / this.blockSize) - this.boundaryGutter,
      ) * this.blockSize;

      // Choose a color if needed
      if (this.colored) {
        this.color = SPECIAL_FOOD_COLORS[getRandomInt(0, SPECIAL_FOOD_COLORS.length)];
      }

      clearTimeout(this.timeoutId);
      this.timeoutId = setTimeout(() => {
        this.put(boundaries);
      }, getRandomInt(this.timeouts[0], this.timeouts[1]));
    }

    /**
     * Check for collision of the eatable object with passed point
     * @param x
     * @param y
     * @returns {boolean}
     */
    checkCollision(x, y) {
      return this.x === x && this.y === y;
    }

    /**
     * Reset eatable object
     */
    reset() {
      clearTimeout(this.timeoutId);
    }
  }

  /** Representing a regular food */
  class Food extends Eatable {
    /**
     * Create a regular food
     * @param {number} blockSize Size of point on game canvas
     * @param {number} size Size of eatable object
     */
    constructor(blockSize, size) {
      super(blockSize, size, [4000, 10000], 0, false);
    }

    /**
     * Draw the food
     * @param ctx Canvas context
     */
    draw(ctx) {
      const x = this.x;
      const y = this.y;
      const size = this.size;

      ctx.beginPath();
      ctx.moveTo(x, y + size / 2);
      ctx.lineTo(x + size / 2, y);
      ctx.lineTo(x + size, y + size / 2);
      ctx.lineTo(x + size / 2, y + size);
      ctx.lineTo(x, y + size / 2);

      ctx.lineWidth = 2;
      ctx.strokeStyle = '#000066';
      ctx.stroke();
      ctx.fillStyle = '#4388cc';
      ctx.fill();
    }
  }

  /** Representing a special food */
  class SpecialFood extends Eatable {
    /**
     * Create a special food
     * @param {number} blockSize Size of point on game canvas
     * @param {number} size Size of eatable object
     */
    constructor(blockSize, size) {
      super(blockSize, size, [1000, 5000], 1, true);
    }
    /**
     * Draw the food
     * @param ctx Canvas context
     */
    draw(ctx) {
      ctx.beginPath();
      ctx.arc(this.x + this.blockSize / 2, this.y + this.blockSize / 2, this.size / 2 - 1, 0, 2 * Math.PI);
      ctx.lineWidth = 1;
      ctx.fillStyle = this.color;
      ctx.fill();
      ctx.stroke();
    }
  }

  // Expose objects to global scope
  window.game.Boundaries = Boundaries;
  window.game.Food = Food;
  window.game.SpecialFood = SpecialFood;
})();